'''FOLLOW THE SEQUENCE TO UNDERSTAND THE WORKING OF THIS CODE 
   Basic reqirement is of chrome_driver  location
'''
import key
import csv
import random
import os
import time
import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

#randomly selects title and location from key.py file
title = random.choice(list(key.grp.values()))
area_of_intrs = random.choice(key.locs)
#defining chrome option
option = Options()
d = DesiredCapabilities.CHROME
d['loggingPrefs'] = { 'browser':'ALL' }
d["pageLoadStrategy"] = "none"  #  complete
option = Options()
option.add_argument("--headless")
option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")
option.add_argument("--disable-images")
option.add_argument("--no-sandbox")
option.add_experimental_option("prefs", {
   "profile.default_content_setting_values.notifications": 2,
   "profile.managed_default_content_settings.images":2
})
#Chrome_driver  location needed
path = " "  #chrome_driver path
#PATH ADDED TO CHROME 
driver = webdriver.Chrome(chrome_options=option, executable_path=path)
link ='https://www.facebook.com/search/pages/?q='+area_of_intrs+"%20"+title #SEARCHING PAGE FIRST AND THEN LOGIN
driver.get(link)
time.sleep(3)
email = driver.find_element_by_id("email")
email.send_keys("testcase.for.sel@gmail.com")
time.sleep(3)
paswrd = driver.find_element_by_id("pass")
paswrd.send_keys("@world-by-me@")
time.sleep(3)
login_button = driver.find_element_by_id("loginbutton")
login_button.click()
time.sleep(5)
driver.execute_script("window.history.go(-1)")
time.sleep(3)
#------------------SCROLLING-------------------------
for i in range(10):
    driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
    time.sleep(3)
initial_results = driver.find_elements_by_xpath("//div[@id='BrowseResultsContainer']")
#---------------------scraping_facebook page link-------------------CURRENTLY USING XPATH
data_list = []
for i in range(len(initial_results)):
    for card in initial_results[i].find_elements_by_xpath("./div[@class='_3u1 _gli _6pe1 _87m1']"):
        card_link = card.find_element_by_xpath("./div/div[@class='clearfix _ikh']")
        data_list.append(card_link.find_element_by_xpath("./div[@class='_4bl7 _3-90']/a").get_attribute("href"))
        # y = card_link.find_element_by_css_selector("div._pac")
#-------------scraping links of scrolled pages------------        
scrolled_list = []
for indx in range(10):
    xpath_ = "fbBrowseScrollingPagerContainer" + str(indx)
    xpath_ = "//div[@id='{0}']".format(xpath_)
    if driver.find_elements_by_xpath(xpath_):
        scrolled_list.append(driver.find_elements_by_xpath(xpath_))
#----------data_list contains all the scraped links        
for k in range(len(scrolled_list)):
    for card in scrolled_list[k][0].find_elements_by_xpath("./div[@class='_1yt']/div[@class='_3u1 _gli _6pe1 _87m1']"):
        card_link = card.find_element_by_xpath("./div/div[@class='clearfix _ikh']")
        data_list.append(card_link.find_element_by_xpath("./div[@class='_4bl7 _3-90']/a").get_attribute("href"))
#--------------------------------list of links --------       
print(len(data_list),"count")
#-------------serching each scrapped link and fetching data
for j in range(len(data_list)):
    print(data_list[j])
    a_link= str(data_list[j])
    driver.get(a_link)
    time.sleep(5)
    #------------scrolling 10 times
    for i in range(10):
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        time.sleep(3)
    #---------------XPATH TO GET PHONE NUMBER TEXT-----
    phone_results = driver.find_elements_by_xpath(".//div[@class='_4-u2 _u9q _3xaf _4-u8']/div[@class='_2pi9 _2pi2']/div[@class='clearfix _ikh']/div[@class='_4bl9']/div")
    phone_num=[]
    email = []
    for i in phone_results:
        try:
            phone_num = int((i.text).replace(' ', ''))
            print(phone_num)
        except ValueError:
            pass
    driver.execute_script("window.history.go(-1)")    #GOING BACK ONE PAGE
time.sleep(2)
driver.quit()